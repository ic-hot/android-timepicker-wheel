package ic.android.ui.view.timepicker.wheel


import java.util.Calendar
import java.util.Locale

import kotlin.collections.ArrayList

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout

import ic.android.ui.view.group.ext.inflateChild
import ic.base.primitives.int32.ext.asFloat32
import ic.util.time.Time
import ic.util.time.funs.now


class WheelTimePicker : FrameLayout {


	private var textSizePxField : Int = 0
	var textSizePx : Int
		get() = textSizePxField
		set(value) {
			textSizePxField = value
			updateTextSize()
		}
	;

	private fun updateTextSize() {
		hourLoopView.setTextSize(textSizePxField.asFloat32)
		minLoopView.setTextSize(textSizePxField.asFloat32)
	}


	var hourList = ArrayList<String>()
	var minList = ArrayList<String>()
	var secList = ArrayList<String>()


	private var hourPos = 0
	private var minPos = 0


	private fun initView() {

		//do not loop,default can loop
		hourLoopView.setNotLoop()
		minLoopView.setNotLoop()

		//set checked listen
		hourLoopView.setListener { item ->
			hourPos = item
		}
		minLoopView.setListener { item ->
			minPos = item
		}
		initPickerViews() // init year and month loop view
	}


	/**
	 * Init year and month loop view,
	 * Let the day loop view be handled separately
	 */
	private fun initPickerViews() {
		val hourCount: Int = 23
		val minCount: Int = 59
		val secCount: Int = 59
		for (i in 0..hourCount) {
			hourList.add(format2LenStr(i))
		}
		for (j in 0..minCount) {
			minList.add(format2LenStr(j))
		}
		for (k in 0..secCount) {
			secList.add(format2LenStr(k))
		}
		hourLoopView.setArrayList(hourList as ArrayList?)
		hourLoopView.setInitPosition(hourPos)
		minLoopView.setArrayList(minList as ArrayList?)
		minLoopView.setInitPosition(minPos)
	}


	private val hourLoopView: LoopView
	private val minLoopView: LoopView


	init {

		val view = inflateChild(R.layout.timepicker_wheel)

		hourLoopView = view.findViewById(R.id.picker_hour)
		minLoopView = view.findViewById(R.id.picker_min)

		initView()

		initPickerViews()

		updateTextSize()

		addView(view)

		value = now()

	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
		: super(context, attrs, defStyleAttr)
	{
		if (attrs != null) {
			val styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.WheelTimePicker)
			textSizePx = styledAttributes.getDimensionPixelSize(R.styleable.WheelTimePicker_textSize, textSizePxField)
			styledAttributes.recycle()
		}
	}


	private fun format2LenStr(num: Int) : String {
		return if (num < 10) "0$num" else num.toString()
	}


	var value : Time

		get() {
			val hour = hourPos
			val min = minPos
			val sb = StringBuffer()
			sb.append(hour.toString())
			sb.append(":")
			sb.append(format2LenStr(min))
			return Time(
				initialTime = now(),
				hourOfDay = hour,
				minute = min
			)
		}

		set(value) {
			val milliseconds: Long = value.epochMs
			val calendar: Calendar = Calendar.getInstance(Locale.getDefault())
			if (milliseconds != -1L) {
				calendar.timeInMillis = milliseconds
				hourPos = calendar.get(Calendar.HOUR_OF_DAY)
				minPos = calendar.get(Calendar.MINUTE)
				hourLoopView.setInitPosition(hourPos)
				minLoopView.setInitPosition(minPos)
			}
		}

	;


}