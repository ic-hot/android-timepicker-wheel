package ic.android.ui.view.timepicker.wheel;

public interface LoopListener {
	void onItemSelect(int item);
}